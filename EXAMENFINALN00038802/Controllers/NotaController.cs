﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EXAMENFINALN00038802.Models;
using EXAMENFINALN00038802.Repository;
using EXAMENFINALN00038802.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EXAMENFINALN00038802.Controllers
{
    [Authorize]
    public class NotaController : Controller
    {
        private readonly IClaimService claim;
        private readonly INotaRepository context;
        public NotaController(INotaRepository context, IClaimService claim)
        {
            this.context = context;
            this.claim = claim;
        }
        [HttpGet]
        public IActionResult Index(string buscar)
        {
            claim.SetHttpContext(HttpContext);
            var notas = context.GetNotas(claim.GetLoggedUser().Id);

            ViewBag.Etiquetas = context.GetEtiquetas();
            ViewBag.Etiquetitas = context.GetEtiquetaNotas();
            var not = context.GetNotas(claim.GetLoggedUser().Id);
            if (!String.IsNullOrEmpty(buscar))
                notas = notas.Where(o => o.Titulo.Contains(buscar) || o.Contenido.Contains(buscar)).ToList();
            return View("Index", notas);
        }

        [HttpGet]
        public IActionResult _Index(string search)
        {
            claim.SetHttpContext(HttpContext);
            var notas = context.GetNotas(claim.GetLoggedUser().Id);
            ViewBag.Etiquetitas = context.GetEtiquetaNotas();
            ViewBag.Etiquetas = context.GetEtiquetas();

            if (!String.IsNullOrEmpty(search))
            {
                notas = notas.Where(o => o.Titulo.Contains(search) || o.Contenido.Contains(search)).ToList();
                return View(notas);
            }
            return View(notas);
        }

        [HttpGet]
        public IActionResult Categoria(int IdCategoria)
        {
            claim.SetHttpContext(HttpContext);
            var categoria = context.GetNotas(claim.GetLoggedUser().Id);

            ViewBag.Etiquetitas = context.GetEtiquetaNotas();
            ViewBag.Etiquetas = context.GetEtiquetas();

            var etiqueta = context.GetCategoriaNotas(claim.GetLoggedUser().Id, IdCategoria);
            return View("Categoria", etiqueta);
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Etiquetas = context.GetEtiquetas();
            return View(new Nota());
        }
        [HttpPost]
        public IActionResult Create(Nota nota, List<int> etiqueta)
        {
            List<EtiquetaNota> etic = new List<EtiquetaNota>();

            if (etiqueta.Count() == 0)
                ModelState.AddModelError("etiqueta", "Escoja una opción");

            nota.Fecha = DateTime.Now;
            claim.SetHttpContext(HttpContext);
            nota.IdUser = claim.GetLoggedUser().Id;
            if (ModelState.IsValid)
            {
                context.GuardarNota(nota);
                foreach (var item in etiqueta)
                {
                    var etique = new EtiquetaNota();
                    etique.IdEtiqueta = item;
                    etique.IdNota = nota.Id;
                    etic.Add(etique);
                }
                context.GuardarEtiqueNota(etic);
                return RedirectToAction("Index");
            }
            else
            {
                Response.StatusCode = 400;
                ViewBag.Etiquetas = context.GetEtiquetas();
                return View("Create", new Nota());
            }

        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            ViewBag.Etiquetas = context.GetEtiquetas();
            var nota = context.GetNota(id);
            return View(nota);

        }
        [HttpPost]
        public IActionResult Edit(Nota nota, int IdEti, List<int> etiqueta)
        {
            nota.Fecha = DateTime.Now;
            List<EtiquetaNota> etic = new List<EtiquetaNota>();

            if (etiqueta.Count() == 0)
                ModelState.AddModelError("etiqueta", "Seleccione por lo menos una");
            claim.SetHttpContext(HttpContext);
            nota.IdUser = claim.GetLoggedUser().Id;
            if (ModelState.IsValid)
            {
                context.EliminaEtiquetas(IdEti);
                context.ActNota(nota);
                foreach (var item in etiqueta)
                {
                    var etique = new EtiquetaNota();
                    etique.IdEtiqueta = item;
                    etique.IdNota = nota.Id;
                    etic.Add(etique);
                }
                context.GuardarEtiqueNota(etic);
                return RedirectToAction("Index");
            }
            ViewBag.Etiquetas = context.GetEtiquetas();
            return View("Edit", nota);
        }
        [HttpGet]
        public IActionResult Det(int id)
        {
            var etiqueta = context.GetEtiquetas();
            ViewBag.Etiquetas = context.GetEtiquetaNotas();
            var nota = context.GetNota(id);
            return View(nota);
        }
        [HttpGet]
        public IActionResult Eliminar(int id)
        {
            context.EliminaNota(id);
            return RedirectToAction("Index");
        }
    }
}
