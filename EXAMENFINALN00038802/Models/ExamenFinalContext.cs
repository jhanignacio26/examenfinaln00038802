﻿using EXAMENFINALN00038802.Models.Maps;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EXAMENFINALN00038802.Models
{
    public interface IExamenFinalContext
    {
        DbSet<Usuario> Usuarios { get; set; }
        DbSet<Nota> Notas { get; set; }
        int SaveChanges();
    }
    public class ExamenFinalContext : DbContext, IExamenFinalContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Nota> Notas { get; set; }
        public DbSet<Etiqueta> Etiquetas { get; set; }
        public DbSet<EtiquetaNota> EtiquetaNotas { get; set; }

        public ExamenFinalContext(DbContextOptions<ExamenFinalContext> options)
    : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new NotaMap());
            modelBuilder.ApplyConfiguration(new EtiquetaMap());
            modelBuilder.ApplyConfiguration(new EtiquetaNotaMap());
        }
    }
}
