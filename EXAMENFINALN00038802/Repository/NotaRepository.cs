﻿using EXAMENFINALN00038802.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EXAMENFINALN00038802.Repository
{
    public interface INotaRepository
    {
        List<Etiqueta> GetEtiquetas();
        Nota GetNota(int id);
        List<EtiquetaNota> GetEtiquetaNotas();
        List<EtiquetaNota> GetCategoriaNotas(int IdUser, int IdCategoria);
        List<Nota> GetNotas(int idUser);

        void GuardarNota(Nota nota);
        void ActNota(Nota nota);
        void GuardarEtiqueNota(List<EtiquetaNota> et);

        void EliminaNota(int id);
        void EliminaEtiquetas(int id);
    }
    public class NotaRepository : INotaRepository

    {
        private readonly ExamenFinalContext context;

        public NotaRepository(ExamenFinalContext context)
        {
            this.context = context;

        }
        public Nota GetNota(int id)
        {
            return context.Notas.Where(o => o.Id == id).FirstOrDefault();
        }

        public List<EtiquetaNota> GetEtiquetaNotas()
        {
            return context.EtiquetaNotas
                .Include(o => o.Etiqueta)
                .Include(o => o.Nota)
                .ToList();
        }

        public List<Etiqueta> GetEtiquetas()
        {
            return context.Etiquetas.ToList();
        }
        public List<Nota> GetNotas(int idUser)
        {
            return context.Notas.Where(o => o.IdUser == idUser).ToList();

        }
        public void GuardarNota(Nota nota)
        {
            context.Notas.Add(nota);
            context.SaveChanges();
        }
        public void GuardarEtiqueNota(List<EtiquetaNota> et)
        {
            context.EtiquetaNotas.AddRange(et);
            context.SaveChanges();
        }
        public void EliminaNota(int id)
        {
            var nota = context.Notas.Where(o => o.Id == id).FirstOrDefault();
            var etiqueta = context.EtiquetaNotas.Where(o => o.IdNota == id).ToList();
            context.Notas.Remove(nota);
            context.EtiquetaNotas.RemoveRange(etiqueta);
            context.SaveChanges();

        }
        public void EliminaEtiquetas(int id)
        {
            var etiquetta = context.EtiquetaNotas.Where(o => o.Id == id).FirstOrDefault();
            context.EtiquetaNotas.RemoveRange(etiquetta);
        }
        public void ActNota(Nota nota)
        {

            context.Notas.Update(nota);
            context.SaveChanges();
        }

        public List<EtiquetaNota> GetCategoriaNotas(int IdUser, int IdCategoria)
        {
            return context.EtiquetaNotas
                .Include(o => o.Etiqueta)
                .Include(o => o.Nota)
                .Where(o => o.Nota.IdUser == IdUser && o.IdEtiqueta == IdCategoria)
                .ToList();
        }
    }
}

