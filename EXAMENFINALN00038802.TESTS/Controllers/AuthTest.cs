﻿using EXAMENFINALN00038802.Controllers;
using EXAMENFINALN00038802.Models;
using EXAMENFINALN00038802.Repository;
using EXAMENFINALN00038802.Service;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAMENFINALN00038802.TESTS.Controllers
{
    [TestFixture]
    public class AuthTest
    {
        [Test]
        public void Caso1_LoginObtener()
        {
            var controller = new AuthController(null, null);
            var view = controller.Login() as ViewResult;

            Assert.AreEqual("Login", view.ViewName);
        }
        [Test]
        public void Caso2_LoginPost_Correctamente()
        {
            var repo = new Mock<IAuthRepository>();
            repo.Setup(o => o.GetUsuario("Usuario", "usuario")).Returns(new Usuario());

            var claim = new Mock<IClaimService>();

            var controller = new AuthController(repo.Object, claim.Object);
            var view = controller.Login("Usuario", "usuario") as RedirectToActionResult;

            Assert.AreEqual("Index", view.ActionName);
        }


        [Test]
        public void Caso3_Registrar_Obtenner()
        {
            var controller = new AuthController(null, null);
            var view = controller.Registrar() as ViewResult;

            Assert.AreEqual("Registrar", view.ViewName);
        }

        [Test]
        public void Caso4_RegistrarPost_Correctamente()
        {
            var repo = new Mock<IAuthRepository>();
            repo.Setup(o => o.GetUsuarios()).Returns(new List<Usuario>());
            repo.Setup(o => o.SaveUsuario(new Usuario()));
            var claim = new Mock<IClaimService>();

            var controller = new AuthController(repo.Object, claim.Object);
            var view = controller.Registrar(new Usuario() { Password = "usuario" }, "usuario") as RedirectToActionResult;

            Assert.AreEqual("Login", view.ActionName);
        }

        [Test]
        public void Caso5_RegistrarPost_Fallido()
        {
            var repo = new Mock<IAuthRepository>();
            repo.Setup(o => o.GetUsuarios()).Returns(new List<Usuario>());

            var claim = new Mock<IClaimService>();

            var controller = new AuthController(repo.Object, claim.Object);
            var view = controller.Registrar(new Usuario() { Password = "1223" }, "1234") as ViewResult;

            Assert.AreEqual("Registrar", view.ViewName);
        }
    }
}
