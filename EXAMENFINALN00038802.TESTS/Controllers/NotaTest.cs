﻿using EXAMENFINALN00038802.Controllers;
using EXAMENFINALN00038802.Models;
using EXAMENFINALN00038802.Repository;
using EXAMENFINALN00038802.Service;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAMENFINALN00038802.TESTS.Controllers
{
    [TestFixture]
    public class NotaTest
    {
        [Test]
        public void Caso1_Index()
        {
            var claim = new Mock<IClaimService>();
            claim.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repo = new Mock<INotaRepository>();
            repo.Setup(o => o.GetEtiquetas()).Returns(new List<Etiqueta>());
            repo.Setup(o => o.GetEtiquetaNotas()).Returns(new List<EtiquetaNota>());
            repo.Setup(o => o.GetNotas(1)).Returns(new List<Nota>());

            var controller = new NotaController(repo.Object, claim.Object);
            var view = controller.Index(null) as ViewResult;

            Assert.AreEqual("Index", view.ViewName);
        }

        [Test]
        public void Caso2_Categoria_Correctamente()
        {
            var claim = new Mock<IClaimService>();
            claim.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repo = new Mock<INotaRepository>();
            repo.Setup(o => o.GetEtiquetas()).Returns(new List<Etiqueta>());
            repo.Setup(o => o.GetEtiquetaNotas()).Returns(new List<EtiquetaNota>());
            repo.Setup(o => o.GetCategoriaNotas(1, 1)).Returns(new List<EtiquetaNota>());

            var controller = new NotaController(repo.Object, claim.Object);
            var view = controller.Categoria(1) as ViewResult;

            Assert.AreEqual("Categoria", view.ViewName);
        }

        [Test]
        public void Caso3_CreatePost()
        {
            var claim = new Mock<IClaimService>();
            claim.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repo = new Mock<INotaRepository>();
            repo.Setup(o => o.GuardarNota(new Nota()));
            repo.Setup(o => o.GuardarEtiqueNota(new List<EtiquetaNota>()));

            var controller = new NotaController(repo.Object, claim.Object);
            var view = controller.Create(new Nota(), new List<int>() { 1 }) as RedirectToActionResult;

            Assert.AreEqual("Index", view.ActionName);
        }

        [Test]
        public void Caso4_EditPost()
        {
            var claim = new Mock<IClaimService>();
            claim.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repo = new Mock<INotaRepository>();
            repo.Setup(o => o.EliminaEtiquetas(1));
            repo.Setup(o => o.ActNota(new Nota()));
            repo.Setup(o => o.GuardarEtiqueNota(new List<EtiquetaNota>()));

            var controller = new NotaController(repo.Object, claim.Object);
            var view = controller.Edit(new Nota(), 1, new List<int>() { 1 }) as RedirectToActionResult;

            Assert.AreEqual("Index", view.ActionName);
        }

        [Test]
        public void Caso5_Eliminar()
        {
            var claim = new Mock<IClaimService>();
            claim.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repo = new Mock<INotaRepository>();

            var controller = new NotaController(repo.Object, claim.Object);
            controller.Eliminar(1);

            Assert.AreEqual(null, null);
        }
    }
}
